
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Test;

public class MainTest {

    @Test
    public void tarjetaValida() {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2030, Month.JANUARY, 20));
        assertTrue(visa.tarjetaValida());
    }

    @Test
    public void tarjetaInvalida() {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2000, Month.JANUARY, 20));
        assertFalse(visa.tarjetaValida());
    }

    @Test
    public void tarjetasIguales() {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2000, Month.JANUARY, 20));
        Visa visa2 = new Visa("Nicolas Reinaldo", LocalDate.of(2000, Month.JANUARY, 20));

        assertTrue(visa.tarjetasIguales(visa2));
    }

    @Test
    public void tarjetasDiferentesEnMarca() {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2000, Month.JANUARY, 20));

        Nara nara = new Nara("Nicolas Reinaldo", LocalDate.of(2024, Month.JANUARY, 20));

        assertFalse(visa.tarjetasIguales(nara));
    }

    @Test
    public void tarjetasDiferentesEnDueño() {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2000, Month.JANUARY, 20));

        Visa visa2 = new Visa("Nico AA", LocalDate.of(2000, Month.JANUARY, 20));

        assertFalse(visa.tarjetasIguales(visa2));
    }

    @Test
    public void tarjetasDiferentesEnFechaVencimiento() {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2000, Month.JANUARY, 20));

        Visa visa2 = new Visa("Nicolas Reinaldo", LocalDate.of(2020, Month.JANUARY, 20));

        assertFalse(visa.tarjetasIguales(visa2));
    }

    @Test
    public void operacionValida() {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2030, Month.JANUARY, 20));

        assertTrue(visa.operacionValida(5000));
    }

    @Test
    public void operacionInvalida() {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2030, Month.JANUARY, 20));

        assertFalse(visa.operacionValida(100));
    }

    @Test(expected = Exception.class)
    public void operacionInvalidaConExcepcion() {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2030, Month.JANUARY, 20));

        visa.operacionValida(-100);
    }

}
