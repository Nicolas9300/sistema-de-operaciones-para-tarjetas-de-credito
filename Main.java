import java.time.LocalDate;
import java.time.Month;

public class Main {

    public static void main(String[] args) {

        Visa visa = new Visa("Nicolas Reinaldo", LocalDate.of(2000, Month.JANUARY, 20));

        System.out.println(visa.toString());
        System.out.println(visa.obtenerTasa(10));
        System.out.println(visa.operacionValida(500));
        System.out.println(visa.tarjetaValida());

        Nara nara = new Nara("Nicolas Reinald", LocalDate.of(2024, Month.JANUARY, 20));

        System.out.println(nara.tarjetasIguales(visa));

    }

}
