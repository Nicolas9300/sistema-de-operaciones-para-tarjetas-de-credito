import java.time.LocalDate;

public abstract class Tarjeta {

    private String _marca;
    protected String _cardholder;
    protected LocalDate _fVencimiento;

    public Tarjeta(String marca, String cardholder, LocalDate fVencimiento) {

        _marca = marca;
        _cardholder = cardholder;
        _fVencimiento = fVencimiento;
    }

    public String get_marca() {
        return _marca;
    }

    public String get_cardholder() {
        return _cardholder;
    }

    public LocalDate get_fVencimiento() {
        return _fVencimiento;
    }

    public abstract String toString();

    public abstract String obtenerTasa(double importe);

    public boolean tarjetaValida() {

        if (_fVencimiento.compareTo(LocalDate.now()) > 0)
            return true;
        return false;
    }

    public boolean operacionValida(double consumision) {

        if (consumision <= 0)
            throw new RuntimeException("Numero incorrecto debes tener una consumision > 0");

        return consumision < 1000 ? false : true;
    }

    public boolean tarjetasIguales(Tarjeta tarjeta) {
        if (_marca.equals(tarjeta.get_marca())
                && _cardholder.equals(tarjeta.get_cardholder())
                && _fVencimiento.compareTo(tarjeta.get_fVencimiento()) == 0) {
            return true;
        }
        return false;
    }

}
