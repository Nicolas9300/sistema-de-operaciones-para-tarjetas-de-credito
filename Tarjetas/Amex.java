
import java.time.LocalDate;

public class Amex extends Tarjeta {

    public Amex(String cardholder, LocalDate fVencimiento) {
        super("AMEX", cardholder, fVencimiento);
    }

    @Override
    public String toString() {
        return "-------AMEX-------" + "\n" + "Dueño: " + super._cardholder + "\n" + "Fecha Vencimiento: "
                + super._fVencimiento;
    }

    @Override
    public String obtenerTasa(double importe) {

        if (importe <= 0)
            throw new RuntimeException("Consumision Invalida el importe tiene que ser > 0");

        LocalDate fechaActual = LocalDate.now();

        int mes = fechaActual.getMonthValue();

        return "-------AMEX------- " + "\n" + "Tasa: " + "\n" + (double) (mes * 0.1) + "\n" + "Importe: " + "\n"
                + importe;

    }

}
