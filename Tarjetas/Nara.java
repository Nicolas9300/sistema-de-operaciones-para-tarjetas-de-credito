
import java.time.LocalDate;

public class Nara extends Tarjeta {

    public Nara(String cardholder, LocalDate fVencimiento) {
        super("NARA", cardholder, fVencimiento);
    }

    @Override
    public String toString() {
        return "-------NARA-------" + "\n" + "Dueño: " + super._cardholder + "\n" + "Fecha Vencimiento: "
                + super._fVencimiento;
    }

    @Override
    public String obtenerTasa(double importe) {

        if (importe <= 0)
            throw new RuntimeException("Consumision Invalida el importe tiene que ser > 0");

        LocalDate fechaActual = LocalDate.now();

        int mes = fechaActual.getMonthValue();

        return "-------NARA------- " + "\n" + "Tasa: " + "\n" + (double) (mes * 0.5) + "\n" + "Importe: " + "\n"
                + importe;

    }

}
