
import java.time.LocalDate;

public class Visa extends Tarjeta {

    public Visa(String cardholder, LocalDate localDate) {
        super("VISA", cardholder, localDate);
    }

    @Override
    public String toString() {
        return "-------VISA-------" + "\n" + "Dueño: " + super._cardholder + "\n" + "Fecha Vencimiento: "
                + super._fVencimiento;
    }

    @Override
    public String obtenerTasa(double importe) {

        if (importe <= 0)
            throw new RuntimeException("Consumision Invalida el importe tiene que ser > 0");

        LocalDate fechaActual = LocalDate.now();

        int mes = fechaActual.getMonthValue();
        int año = fechaActual.getYear();

        return "-------VISA------- " + "\n" + "Tasa: " + "\n" + (double) (año / mes) + "\n" + "Importe: " + "\n"
                + importe;

    }

}
